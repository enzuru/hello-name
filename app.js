const express = require('express')
const name = require('./name')
const app = express()
const port = process.env.PORT || 8080

app.get('/', (req, res) => {
  res.send('Hello ' + name)
})

app.listen(port, () => console.log(`App listening on port ${port}!`))
