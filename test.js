const assert = require('assert')
const name = require('./name')

describe('name', function () {
  it('should have correct name', function () {
    assert.equal(name, 'Grayson')
  })
})
